package sk.gopas.android2

import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import sk.gopas.android2.broadcast.BroadcastFragment
import sk.gopas.android2.coroutines.CoroutinesFragment
import sk.gopas.android2.http.HttpFragment
import sk.gopas.android2.location.LocationFragment
import sk.gopas.android2.maps.MapsFragment
import sk.gopas.android2.sensor.SensorsFragment
import sk.gopas.android2.service.ServiceFragment
import sk.gopas.android2.web.WebFragment
import sk.gopas.android2.work.WorkFragment
import sk.gopas.android2.databinding.ActivityMainBinding
import timber.log.Timber

class MainActivity : AppCompatActivity() {
	private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
	private val drawerToggle by lazy {
		object : ActionBarDrawerToggle(this, binding.drawerLayout, R.string.drawer_open, R.string.drawer_close) {
			override fun onDrawerOpened(drawerView: View) {
				super.onDrawerOpened(drawerView)
				invalidateOptionsMenu()
			}

			override fun onDrawerClosed(drawerView: View) {
				super.onDrawerClosed(drawerView)
				invalidateOptionsMenu()
			}
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(binding.root)

		setSupportActionBar(binding.toolbar)

		binding.drawerLayout.addDrawerListener(drawerToggle)

		with(supportActionBar!!) {
			setDisplayHomeAsUpEnabled(true)
			setHomeButtonEnabled(true)
		}

		binding.drawer.setNavigationItemSelectedListener {
			supportFragmentManager.commit {
				when (it.itemId) {
					R.id.coroutines -> replace(R.id.fragments, CoroutinesFragment())
					R.id.web -> replace(R.id.fragments, WebFragment())
					R.id.http -> replace(R.id.fragments, HttpFragment())
					R.id.maps -> replace(R.id.fragments, MapsFragment())
					R.id.location -> replace(R.id.fragments, LocationFragment())
					R.id.sensors -> replace(R.id.fragments, SensorsFragment())
					R.id.broadcasts -> {
						Firebase.crashlytics.log("Test log")
						Firebase.crashlytics.setCustomKey("paid", true)
						Firebase.crashlytics.recordException(Exception("Test, not critical"))
						TODO("In services")
					}
					R.id.service -> replace(R.id.fragments, ServiceFragment())
					R.id.work -> replace(R.id.fragments, WorkFragment())
					else -> Timber.w("Unknown item")
				}
			}
			binding.drawerLayout.closeDrawer(binding.drawer)
			true
		}

		lifecycleScope.launch {
			Timber.i("Token: ${Firebase.messaging.token.await()}")
		}
	}

	override fun onPostCreate(savedInstanceState: Bundle?) {
		super.onPostCreate(savedInstanceState)
		drawerToggle.syncState()
	}

	override fun onOptionsItemSelected(item: MenuItem) = drawerToggle.onOptionsItemSelected(item)

	override fun onConfigurationChanged(newConfig: Configuration) {
		super.onConfigurationChanged(newConfig)
		drawerToggle.onConfigurationChanged(newConfig)
	}

	override fun onBackPressed() {
		if (binding.drawerLayout.isDrawerOpen(binding.drawer)) {
			binding.drawerLayout.closeDrawer(binding.drawer)
		} else {
			super.onBackPressed()
		}
	}
}
