package sk.gopas.android2.coroutines

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import sk.gopas.android2.BaseFragment
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class CoroutinesFragment : BaseFragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d("Is application context? ${context.applicationContext == context}")

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Default) {
            val downloads = sendRequest()
            withContext(Dispatchers.Main) {
                text.text = "$downloads"
            }
        }
    }

    suspend fun sendRequest() = suspendCoroutine<String> { cont ->
        cont.resume("df")
    }

    override fun onResume() {
        super.onResume()
        FRAGMENTS += this
        Timber.d("Got ${FRAGMENTS.size} fragments")
        Timber.d("context = activity.context ${requireContext() === requireActivity()}")

                           lifecycleScope.launch {  }
        viewLifecycleOwner.lifecycleScope.launch { // Has UI }
    }

    private companion object {
        private val FRAGMENTS = mutableListOf<Fragment>()
    }
}