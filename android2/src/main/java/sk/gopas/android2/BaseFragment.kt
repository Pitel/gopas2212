package sk.gopas.android2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

abstract class BaseFragment: Fragment() {

	protected val text by lazy { TextView(requireContext()) }

	protected val threadName: String
		get() = Thread.currentThread().name

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View = text
}