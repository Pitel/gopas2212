package sk.gopas.android2.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Binder
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.getSystemService
import timber.log.Timber

class Service: Service() {
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
            val charging = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1) == BatteryManager.BATTERY_STATUS_CHARGING
            Timber.d("${100 * level / scale.toFloat()} %, $charging")
        }
    }

    override fun onBind(intent: Intent?) = MyBinder()

    override fun onCreate() {
        super.onCreate()
        Timber.d("create")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, "Gopas", NotificationManager.IMPORTANCE_DEFAULT)
            val notificationManager = getSystemService<NotificationManager>()!!
            notificationManager.createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(android.R.drawable.ic_media_play)
            .setContentTitle("Content title")
            .setContentText("Content text. Lorem ipsum...")
            .build()

        startForeground(NOTIFICATION_ID, notification)

        registerReceiver(receiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("$intent")
        return super.onStartCommand(intent, flags, startId)
    }

    fun hello() = "hello"

    override fun onDestroy() {
        Timber.d("destroy")
        super.onDestroy()
    }

    inner class MyBinder: Binder() {
        val service = this@Service
    }

    private companion object {
        private const val CHANNEL_ID = "gopaschannel"
        private const val NOTIFICATION_ID = 54
    }
}