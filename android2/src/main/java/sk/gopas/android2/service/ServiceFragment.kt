package sk.gopas.android2.service

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.IBinder
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import sk.gopas.android2.BaseFragment
import sk.gopas.android2.aidl.IGreet
import timber.log.Timber

class ServiceFragment : BaseFragment() {
    private val serviceIntent by lazy { Intent(context, Service::class.java) }

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            text.text = (service as Service.MyBinder).service.hello()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Timber.w("Disconnected")
        }
    }

    private val aidlConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            text.text = IGreet.Stub.asInterface(service).greet("Gopas")
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Timber.w("Disconnected")
        }
    }

    private val permissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
        if (granted) {
            ContextCompat.startForegroundService(requireContext(), serviceIntent)
        } else {
            Timber.d("Explain why permission")
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when {
            ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED -> {
                ContextCompat.startForegroundService(requireContext(), serviceIntent)
            }
            shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS) -> {
                Timber.d("Explain why permission")
            }
            else -> {
                permissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        requireContext().bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE)
        requireContext().bindService(Intent("GopasGreet").setPackage("sk.gopas.android2.aidl"), aidlConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onDestroyView() {
        requireContext().unbindService(connection)
        requireContext().unbindService(aidlConnection)
        super.onDestroyView()
    }

    override fun onDetach() {
        requireContext().stopService(serviceIntent)
        super.onDetach()
    }
}