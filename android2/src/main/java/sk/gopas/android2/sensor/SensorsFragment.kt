package sk.gopas.android2.sensor

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.core.content.getSystemService
import sk.gopas.android2.BaseFragment
import timber.log.Timber

class SensorsFragment : BaseFragment() {
    private val manager: SensorManager by lazy { requireContext().getSystemService()!! }

    private val listener = object : SensorEventListener {
        override fun onSensorChanged(event: SensorEvent) {
            text.text = "${event.values.first()} lx"
        }

        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
            Timber.d("Accuracy $accuracy")
        }
    }

    override fun onStart() {
        super.onStart()
        manager.getDefaultSensor(Sensor.TYPE_LIGHT)?.let {sensor ->
            manager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun onStop() {
        manager.unregisterListener(listener)
        super.onStop()
    }
}