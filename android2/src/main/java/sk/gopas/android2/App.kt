package sk.gopas.android2

import android.app.Application
import com.google.android.material.color.DynamicColors
import timber.log.Timber

class App : Application() {
	init {
	    Timber.plant(Timber.DebugTree())
	}
	override fun onCreate() {
		super.onCreate()
		DynamicColors.applyToActivitiesIfAvailable(this)
	}
}