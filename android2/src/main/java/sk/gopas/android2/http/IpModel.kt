package sk.gopas.android2.http

import androidx.annotation.Keep

@Keep
data class IpModel(
    val origin: String
)