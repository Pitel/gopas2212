package sk.gopas.android2.http

import retrofit2.http.GET

interface IpService {
    @GET("/ip")
    suspend fun ip(): IpModel
}