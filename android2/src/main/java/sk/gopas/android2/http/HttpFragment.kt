package sk.gopas.android2.http

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import sk.gopas.android2.BaseFragment

class HttpFragment : BaseFragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewLifecycleOwner.lifecycleScope.launch {
            text.text = HTTPBIN.ip().origin
        }
    }

    private companion object {
        private val OKHTTP = OkHttpClient.Builder()
            .addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()

        private val HTTPBIN = Retrofit.Builder()
            .baseUrl("https://httpbin.org")
            .client(OKHTTP)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(IpService::class.java)
    }
}