package sk.gopas.kalkulacka.history

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder

class HistoryAdapter(private val onClick: (Float) -> Unit) : ListAdapter<HistoryEntity, HistoryAdapter.HistoryViewHolder>(DIFF) {
    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int) = getItem(position).id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = HistoryViewHolder(
        LayoutInflater.from(parent.context).inflate(
            android.R.layout.simple_list_item_1,
            parent,
            false
        )
    ).also { Log.d(TAG, "Create") }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        Log.d(TAG, "Bind $position")
        val entity = getItem(position)
        (holder.itemView as TextView).text = "${entity.value}"
        holder.itemView.setOnClickListener { onClick(entity.value) }
    }

    inner class HistoryViewHolder(itemView: View) : ViewHolder(itemView)

    private companion object {
        private val TAG = HistoryAdapter::class.simpleName
        private val DIFF = object : ItemCallback<HistoryEntity>() {
            override fun areItemsTheSame(
                oldItem: HistoryEntity,
                newItem: HistoryEntity
            ) = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: HistoryEntity,
                newItem: HistoryEntity
            ) = oldItem == newItem
        }
    }
}