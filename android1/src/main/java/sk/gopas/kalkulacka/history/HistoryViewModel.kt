package sk.gopas.kalkulacka.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import kotlinx.coroutines.flow.MutableStateFlow

class HistoryViewModel(application: Application) : AndroidViewModel(application) {
    val selected = MutableStateFlow<Float?>(null)

    val db = Room.databaseBuilder(application, HistoryDatabase::class.java, "history")
        .allowMainThreadQueries() // DO NOT DO IN PRODUCTION!!!
        .build()
        .historyDao()

    private companion object {
        private val TAG = HistoryViewModel::class.simpleName
    }
}