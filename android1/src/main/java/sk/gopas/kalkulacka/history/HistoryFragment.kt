package sk.gopas.kalkulacka.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import sk.gopas.kalkulacka.R

class HistoryFragment : Fragment(R.layout.fragment_history) {
    private val viewModel: HistoryViewModel by viewModels({ requireActivity() })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recycler = view as RecyclerView
        recycler.setHasFixedSize(true)
        val adapter = HistoryAdapter {
            viewModel.selected.value = it
            findNavController().popBackStack()
        }
        recycler.adapter = adapter
        viewModel.db.getAll().onEach {
            adapter.submitList(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
//        adapter.submitList(List(1000) {
//            HistoryEntity(it.toFloat(), it.toLong())
//        })
    }
}