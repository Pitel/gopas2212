package sk.gopas.kalkulacka.about

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import sk.gopas.kalkulacka.R

class AboutFragment : Fragment(R.layout.fragment_about) {
    private val args by navArgs<AboutFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("ARGS", args.name)
    }
}