package sk.gopas.kalkulacka.calc

import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import sk.gopas.kalkulacka.R
import sk.gopas.kalkulacka.databinding.FragmentCalcBinding
import sk.gopas.kalkulacka.history.HistoryEntity
import sk.gopas.kalkulacka.history.HistoryViewModel

class CalcFragment: Fragment() {
    private var binding: FragmentCalcBinding? = null
    private val viewModel: CalcViewModel by viewModels()
    val historyViewModel: HistoryViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCalcBinding.inflate(inflater, container, false)
        .also {
            binding = it
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.run {
            calc.setOnClickListener {
                calc()
            }
            share.setOnClickListener {
                share()
            }
            ans.setOnClickListener {
                viewModel.ans()
            }
            hist.setOnClickListener {
                findNavController().navigate(CalcFragmentDirections.history())
            }
        }

        viewModel.ans.filterNotNull().onEach {
            binding?.aText?.setText(it)
            viewModel.ans.value = null
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.res.filterNotNull().onEach {
            binding?.res?.text = "$it"
            try {
                historyViewModel.db.insert(HistoryEntity(it))
            } catch (sqle: SQLiteConstraintException) {
                Log.w(TAG, sqle)
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    override fun onStart() {
        super.onStart()
        historyViewModel.selected.filterNotNull().onEach {
            binding?.bText?.setText("$it")
            historyViewModel.selected.value = null
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun calc() {
        Log.d(TAG, "Calc!")
        val a = binding?.aText?.text?.toString()?.toFloatOrNull() ?: Float.NaN
        val b = binding?.bText?.text?.toString()?.toFloatOrNull() ?: Float.NaN
        try {
            viewModel.calc(a, b, binding!!.ops.checkedRadioButtonId)
        } catch (iae: IllegalArgumentException) {
            findNavController().navigate(CalcFragmentDirections.zero())
        }
    }

    private fun share() {
        Log.d(TAG, "Share!")
        val intent = Intent(Intent.ACTION_SEND)
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding?.res?.text))
            .setType("text/plain")
        startActivity(intent)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}