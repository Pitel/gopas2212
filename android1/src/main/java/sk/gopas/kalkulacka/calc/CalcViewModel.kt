package sk.gopas.kalkulacka.calc

import android.app.Application
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.preference.PreferenceManager
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import sk.gopas.kalkulacka.R
import kotlin.concurrent.thread

class CalcViewModel(application: Application): AndroidViewModel(application) {
    private val prefs by lazy { PreferenceManager.getDefaultSharedPreferences(application) }

    val ans = MutableStateFlow<String?>(null)

    private val res_ = MutableStateFlow<Float?>(null)
    val res: StateFlow<Float?> = res_

    private fun ans(res: String) {
        prefs.edit {
            putString(RES_KEY, res)
        }
    }

    fun ans() {
        ans.value = prefs.getString(RES_KEY, null)
    }

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        require(op != R.id.div && b != 0f)
        thread {
            Thread.sleep(1000) // Hardcore math
            res_.value = when (op) {
                R.id.add -> a + b
                R.id.sub -> a - b
                R.id.mul -> a * b
                R.id.div -> a / b
                else -> Float.NaN
            }.also {
                ans("$it")
            }
        }
    }

    private companion object {
        private val TAG = CalcViewModel::class.simpleName
        private const val RES_KEY = "res"
    }
}