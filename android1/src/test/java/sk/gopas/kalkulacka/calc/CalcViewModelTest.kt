package sk.gopas.kalkulacka.calc

import io.kotest.assertions.timing.eventually
import sk.gopas.kalkulacka.R
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.mockk
import kotlin.time.Duration.Companion.seconds

class CalcViewModelTest: StringSpec({
    val vm = CalcViewModel(mockk(relaxed = true))

    "add" {
        vm.calc(1f, 1f, R.id.add)
        eventually(5.seconds) {
            vm.res.value shouldBe 2f
        }
    }

    afterTest {
        clearAllMocks()
    }
})