package sk.gopas.android2.aidl

import android.app.Service
import android.content.Intent
import sk.gopas.android2.aidl.IGreet.Stub

class GreetService : Service() {
    private val binder = object : Stub() {
        override fun greet(name: String) = "Hello $name"
    }

    override fun onBind(intent: Intent?) = binder
}